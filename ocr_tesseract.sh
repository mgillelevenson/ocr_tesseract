#!/bin/bash

cd in 

echo -n "Fichier ${j}: quelle est la langue à reconnaître ? \nRéponse multiple possible (séparer les entrées par un '+') [lat/eng/spa/fra/spa_old/ita/cat] "
read langue

echo -n "Quelle résolution voulez-vous ? (Recommandé: 200)"
read resolution

for j in *.pdf; do
rm ../.tmp/*
nomfichier=$(basename -- "$j")
nomfichier_sansextension="${nomfichier%.*}"
mv $j ../.tmp
cd ../.tmp
pdftk $j burst
rm $j

for i in *.pdf; do
filename_extension=$(basename -- "$i")
filename_seul="${filename_extension%.*}"
convert -density $resolution $i $filename_seul.jpeg
rm $i
tesseract -l $langue $filename_seul.jpeg $filename_seul pdf
rm $filename_seul.jpeg; 
done 

pdftk *.pdf cat output ../out/$nomfichier
rm pg_*.pdf
cd ../in;
done
print "Travail terminé !"




