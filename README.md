# OCR_tesseract

Ce dépôt contient un script qui permet d'automatiser l'utilisation de tesseract sur un fichier pdf. 

## Dépendances

Le script utilise les programmes ImageMagick, PDFtk pour effectuer des conversions de format. Tesseract est bien sûr utilisé pour la reconnaissance de caractères.

Toutes les dépendances sont installées avec: `sudo make` 

## Utilisation

Il suffit de déposer le ou les fichiers pdf à ocriser dans le dossier in/ (attention aux espaces dans le nom du fichier), puis de lancer le script: `sh ocr_tesseract.sh`, et de suivre les instructions. 

## Licence


### Tesseract

  Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

### ImageMagick

   Licensed under the ImageMagick License (the "License"); you may not use
   this file except in compliance with the License.  You may obtain a copy
   of the License at

     https://imagemagick.org/script/license.php

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
   WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
   License for the specific language governing permissions and limitations
   under the License.

