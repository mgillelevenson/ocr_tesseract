all:
	add-apt-repository ppa:malteworld/ppa
	apt update
	apt install pdftk
	apt install imagemagick
	apt install tesseract-ocr tesseract-ocr-spa tesseract-ocr-spa-old tesseract-ocr-fra tesseract-ocr-ita tesseract-ocr-eng 
	chmod +x ocr_tesseract.sh 
	mkdir in/ 
	mkdir .tmp/ 
	mkdir out/ 

